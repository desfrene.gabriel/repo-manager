#!/usr/bin/env python3

# Installer of Desfrene Repository
# Copyright (C) 2022  Gabriel Desfrene <desfrene.gabriel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import getpass
import logging
import os
import re
import subprocess
import sys
from pathlib import Path
from shutil import which, rmtree
from time import sleep

repo_dir = Path("/var/debian")
mini_dinstall_dir = repo_dir / "mini-dinstall"

log_file = mini_dinstall_dir / "repo-manager.log"

gpg_dir = mini_dinstall_dir / "key"
website_dir = Path("/var/www/repo")

master_keyring = gpg_dir / "repo-keyring.gpg"
install_repo_script = repo_dir / "install"

gpg_version_re = re.compile(r"^gpg\D*(\d*)\.(?:\d.)*$", re.MULTILINE)

gpg_sign_key_re = re.compile(
    r"^sec(?::[^:]*){3}:([0-9A-F]{16})(?::[^:]*){6}:\w*S\w*(?::[^:]*){8}:$", re.MULTILINE)

a2ensite = which("a2ensite")
a2dissite = which("a2dissite")
apachectl = which("apachectl")
gpg = which("gpg")
gpgconf = which("gpgconf")
less = which("less")

ressource_install_repo_script = Path(__file__).parent / "install_repo_script"


def check_gpg():
    if gpg is None:
        logging.error(f"'gpg' executable not found.")
        exit(1)

    if gpg_dir.owner() != getpass.getuser():
        logging.error(f"Wrong owner of gpg keys directory.")
        exit(2)

    if (gpg_dir.stat().st_mode & 0o777) != 0o700:
        logging.error(f"Wrong permissions of gpg keys directory.")
        exit(2)


def get_gpg_signing_keys() -> list[str]:
    output = subprocess.check_output(
        [gpg, "--no-default-keyring", "--homedir", str(gpg_dir), "--keyring", master_keyring.name,
         "--list-secret-keys", "--keyid-format", "long", "--with-colons"])

    return gpg_sign_key_re.findall(output.decode())


def run_gpg(*args):
    check_gpg()
    gpg_options = ["--quiet", "--batch", "--no-tty", "--no-default-keyring", "--homedir",
                   str(gpg_dir), "--keyring", master_keyring.name]

    gpg_version = gpg_version_re.findall(subprocess.check_output([gpg, "--version"]).decode())[0]

    if int(gpg_version) > 1:
        gpg_options += ["--pinentry-mode", "loopback"]

    try:
        subprocess.check_call([gpg] + gpg_options + list(args))
    except subprocess.CalledProcessError as e:
        logging.error(f"Error on executing : {e.cmd} (Exit Code : {e.returncode}) :\n"
                      f"Output : \n"
                      f"{e.output}\n"
                      f"Stderr :\n"
                      f"{e.stderr}")
        exit(5)


def install_repository():
    if not repo_dir.exists():
        repo_dir.mkdir(parents=True)
    else:
        print("Repository yet exists, passing...")

    if not ressource_install_repo_script.exists():
        logging.error(f"Resource Install Script not found !")
        exit(1)

    if install_repo_script.exists():
        install_repo_script.unlink()

    install_repo_script.symlink_to(ressource_install_repo_script)


def install_gpg() -> None:
    if not gpg_dir.exists():
        gpg_dir.mkdir(parents=True)

    gpg_dir.chmod(0o700)

    run_gpg("--fingerprint")
    keys = get_gpg_signing_keys()

    if len(keys) == 0:
        print("No Signing Key Found !")
    else:
        if len(keys) > 1:
            print("Warning : Multiples signings keys found !")
        print(f"Using {keys[0]} key to sign.")


def install_apache_server() -> None:
    if a2ensite is None:
        logging.error(f"'a2ensite' executable not found.")
        exit(1)

    if apachectl is None:
        logging.error(f"'apachectl' executable not found.")
        exit(1)

    if website_dir.exists():
        website_dir.unlink()

    website_dir.symlink_to(repo_dir)

    subprocess.call([a2ensite, "--quiet", "repo-plain.conf"])

    if subprocess.call([apachectl, "restart"]) != 0:
        print("Error on apache restart. Please check apache configuration.")


def uninstall_repository() -> None:
    if gpgconf is None:
        logging.error(f"'gpgconf' executable not found.")
        exit(1)

    subprocess.check_call([gpgconf, "--kill", "gpg-agent"])

    print("Waiting for gpg-agent to shutdown...")
    sleep(1)

    rmtree(repo_dir)
    print("Deleted repository.")


def uninstall_apache_server() -> None:
    if a2dissite is None:
        logging.error(f"'a2dissite' executable not found.")
        exit(1)

    if apachectl is None:
        logging.error(f"'apachectl' executable not found.")
        exit(1)

    if website_dir.exists():
        website_dir.unlink()

    subprocess.call([a2dissite, "--quiet", "repo-plain.conf"])
    subprocess.call([a2dissite, "--quiet", "repo-ssl.conf"])

    if subprocess.call([apachectl, "restart"]) != 0:
        print("Error on apache restart. Please check apache configuration.")


def uninstall_gpg():
    rmtree(gpg_dir)
    print("Deleted gpg repository.")


def sign_release(file_to_sign: Path) -> None:
    if not file_to_sign.exists():
        logging.error(f"{file_to_sign} does not exists.")
        exit(3)

    release_gpg = file_to_sign.parent / "Release.gpg.tmp"

    if release_gpg.exists():
        release_gpg.unlink()

    in_release = file_to_sign.parent / "InRelease.tmp"

    if in_release.exists():
        in_release.unlink()

    run_gpg("--digest-algo", "SHA512", "--detach-sign", "-o", str(release_gpg), str(file_to_sign))
    release_gpg.rename("Release.gpg")

    run_gpg("--digest-algo", "SHA512", "--clearsign", "-o", str(in_release), str(file_to_sign))
    in_release.rename("InRelease")


def gpg_wrapper(*args):
    check_gpg()

    os.execl(gpg, gpg, "--homedir", str(gpg_dir), "--keyring", master_keyring.name,
             "--no-default-keyring", *args)


def show_log():
    if less is None:
        logging.error(f"'less' executable not found.")
        exit(1)

    os.execl(less, less, "+G", "-e", str(mini_dinstall_dir / "mini-dinstall.log"))


def print_usage():
    print(f"""{sys.argv[0]}  Copyright (C) 2022  Desfrene Gabriel <desfrene.gabriel@gmail.com>
This program comes with ABSOLUTELY NO WARRANTY. This is free software,
and you are welcome to redistribute it under certain conditions ; for more
information see GPL-3 License.

Usage :

{sys.argv[0]} [option]

""" + \
          # Installation Options :
          # - install-repo                   Install Debian Repository
          # - install-apache                 Install Apache Website for Repository
          # - install-gpg                    Install Repository GPG Keyring
          # - full-install                   Install Everything (Repository & Website)
          #
          # Uninstallation Options :
          # - uninstall-repo                 Uninstall Debian Repository
          # - uninstall-apache               Uninstall Apache Website
          # - uninstall-gpg                  Uninstall Repository GPG Keyring
          # - full-uninstall                 Uninstall Everything (Repository & Website)
          """Key Management Options :
- gpg                                   GPG wrapper for Trusted Keyring of Repository
- add-key [key-to-add]                  Add a Key to Trusted Keyring of Repository
- del-key [key-fingerprint]             Delete a Key from Trusted Keyring of Repository
- list-key                              List Trusted Keyring of Repository for Verification

Debian Repository Option :
- show-log                              Show log of the repository.

Debian Repository Signing Option :
- [file-to-sign]                        File to sign (If provided path exists)
- signing-key                           Show Used Key to Sign


Configuration Files : 
   - /etc/mini-dinstall.conf            Mini-dInstall Config File
   - /etc/apache2/sites-available/      Web Repository Apache Config Files 
""")


def main():
    if log_file.exists():
        logging.basicConfig(filename=log_file, encoding='utf-8', level=logging.DEBUG)

    logging.debug(f"Starting. argv  {sys.argv}")

    nb_args = len(sys.argv) - 1
    if nb_args == 0:
        print_usage()
    else:
        option = sys.argv[1]
        if option in ["-h", "--help", "help"]:
            print_usage()

        elif option == "install-repo" and nb_args == 1:
            install_repository()

        elif option == "install-apache" and nb_args == 1:
            install_apache_server()

        elif option == "install-gpg" and nb_args == 1:
            install_gpg()

        elif option == "full-install" and nb_args == 1:
            install_repository()
            install_gpg()
            install_apache_server()
            log_file.touch()

        elif option == "uninstall-repo" and nb_args == 1:
            uninstall_repository()

        elif option == "uninstall-apache" and nb_args == 1:
            uninstall_gpg()
            uninstall_apache_server()

        elif option == "uninstall-gpg" and nb_args == 1:
            uninstall_gpg()

        elif option == "full-uninstall" and nb_args == 1:
            uninstall_apache_server()
            uninstall_gpg()
            uninstall_repository()

        elif option == "add-key" and nb_args == 2:
            key_to_trust = Path(sys.argv[2])

            if key_to_trust.exists():
                gpg_wrapper("--import", str(key_to_trust))
            else:
                logging.error(f"{key_to_trust} does not exists.")
                exit(3)

        elif option == "del-key" and nb_args == 2:
            gpg_wrapper("--delete-keys", sys.argv[2])

        elif option == "list-key" and nb_args == 1:
            gpg_wrapper("--list-key")

        elif option == "gpg":
            gpg_wrapper(*sys.argv[2:])

        elif option == "show-log" and nb_args == 1:
            show_log()

        elif option == "signing-key" and nb_args == 1:
            keys = get_gpg_signing_keys()

            if len(keys) == 0:
                print("No Signing Key Found !")
            else:
                if len(keys) > 1:
                    print("Warning : Multiples signings keys found !")
                print(f"Using {keys[0]} to sign.")
                gpg_wrapper("--list-keys", keys[0])

        else:
            logging.debug(f"Signing file : {Path(option)}")
            sign_release(Path(option))

    logging.debug("End !")
