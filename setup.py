from setuptools import setup

setup(
    name="repo-manager",
    version="1.0",
    platforms="any",
    url="https://gitlab.com/desfrene/repo-manager",
    license="GPL-3",
    packages=["repo_manager"],
    package_data={
        "repo_manager": ["install_repo_script"],
    },
    author="Gabriel Desfrene",
    author_email="gabriel@desfrene.fr",
    description="A small utility to manage a Debian Repository.",
    entry_points={
        "console_scripts": [
            "repo-manager=repo_manager:main",
        ]
    },
    python_requires=">=3.9",
)
